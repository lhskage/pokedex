import { PokemonImage } from '../models/pokemonImage.model'
import { Abilities } from '../models/abilities.model'
import { Type } from '../models/type.model'

export interface Pokemon {
    name: string,
    url: string,
    sprites?: PokemonImage,
    types?: Type,
    base_stats?: string,
    height?: number,
    weight?: number,
    abilities?: Abilities,
    base_experience?: number
}