import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../environments/environment'


@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private http: HttpClient) { }

  public getPokemonImage(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
  }

  getPokemon(): Promise<any> {
    return this.http.get(`${environment.pokemonUrl}/?offset=0&limit=151`)
    .toPromise()
  }

  getPokemonById(pokemonName: string): Promise<any> {
    return this.http.get(`${environment.pokemonUrl}/${pokemonName}`)
    .toPromise()
  }
}