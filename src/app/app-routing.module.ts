import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartComponent } from './components/start/start.component';
import { AuthGuard } from './guards/auth.guard'



const routes: Routes = [{
  path: 'start',
  component: StartComponent
},
{
  path: 'trainer',
  loadChildren: () => import('./components/trainer/trainer.module').then(module => module.TrainerModule),
  canActivate: [AuthGuard]
},
{
  path: 'catalogue',
  loadChildren: () => import('./components/catalogue/catalogue.module').then(module => module.CatalogueModule),
  canActivate: [ AuthGuard ]
},
{
  path: 'pokemon/:pokemonId',
  loadChildren: () => import('./components/pokemon-detail/pokemon-detail.module').then(module => module.PokemonDetailModule),
  canActivate: [ AuthGuard ]
},
{
  path: '',
  pathMatch: 'full',
  redirectTo: '/start'
},
{
  path: '**',
  pathMatch: 'full',
  redirectTo: '/start'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
