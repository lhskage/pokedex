import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router'
import { TrainerComponent } from '../trainer/trainer.component'

const routes: Routes = [
    {
        path: '',
        component: TrainerComponent
    }

]

@NgModule({
    imports: [RouterModule.forChild( routes ),
        CommonModule],
    exports: [RouterModule]
})

export class TrainerModule {}