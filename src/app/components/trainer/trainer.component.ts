import { Component, OnInit } from '@angular/core';
import { PokemonStorageService } from '../../services/pokemonStorage/pokemon-storage.service'
import { Pokemon } from '../../models/pokemon.model'
import { Router } from '@angular/router';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})

export class TrainerComponent implements OnInit {

  capturedPokemon: Pokemon = this.pokemonStorage.getCapturedPokemon()

  constructor(private pokemonStorage: PokemonStorageService,
    private router: Router) { 
  }

  ngOnInit(): void {
    console.log("FANGET: ", this.capturedPokemon)
  }

  onPokemonSelected(pokemon: Pokemon) {
    this.pokemonStorage.savePokemon(pokemon)
    this.router.navigateByUrl(`/pokemon/${pokemon.name}`)
  }
}
