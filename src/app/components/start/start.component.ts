import * as uuid from 'uuid';
import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session/session.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {

  constructor(private session: SessionService,
              private router: Router) {
    if (this.session.getLoginToken() !== false) {
      // redirect to dashboard
      this.router.navigateByUrl('/trainer')
    }
  }

  user = {
    token: uuid.v4(),
    username: ''
  }

  isLoading: boolean = false
  
  ngOnInit() {
  }
  
  onRegisterClicked() {

    if(this.user.username !== '') {
      this.session.save(this.user.token, this.user.username)
    } else {
      window.alert('You need a name!')
    }
    this.router.navigateByUrl('/trainer')
  }
}
