import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonStorageService } from '../../services/pokemonStorage/pokemon-storage.service'

import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit {

  pokemon: Pokemon = this.pokemonStorage.getPokemon()

  constructor(private router: Router,
    private pokemonStorage: PokemonStorageService) { }

  ngOnInit(): void {
    console.log("Pokemon valgt: ", this.pokemon)
  }

  onCapturePokemonClicked() {
    if(confirm(`You are trying to catch a ${this.pokemon.name}. Throw Ultraball?`)) {
      this.pokemonStorage.saveCapturedPokemon(this.pokemon)
      this.router.navigateByUrl('/catalogue')
      window.alert(`GOTCHA! \n${this.pokemon.name.toUpperCase()} was caught!`)
    }
  }
}
