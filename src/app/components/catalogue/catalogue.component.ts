import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonService } from '../../services/api/pokemon.service'
import { SessionService } from '../../services/session/session.service'
import { PokemonStorageService } from '../../services/pokemonStorage/pokemon-storage.service'
import { Pokemon } from '../../models/pokemon.model'
import { PokemonImage } from '../../models/pokemonImage.model'

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {

  @Input() pokemon: Pokemon;
  @Output() pokemonName: EventEmitter<string> = new EventEmitter();

  pokemonList: Pokemon[] = []
  selectedPokemon: Pokemon = null

  constructor(private pokemonService: PokemonService,
    private router: Router,
    private pokemonStorageService: PokemonStorageService) {
  }

 async ngOnInit() {
    try {
      const result: any = await this.pokemonService.getPokemon()
      console.log("RESULT ", result.results)
      this.pokemonList = result.results || []
      this.pokemonList.forEach((pokemon)=> {
        const imageUrl: PokemonImage = this.pokemonService.getPokemonImage(pokemon.url)

        pokemon.sprites = imageUrl
      })
    } catch (error) {
      console.error('ERROR: ', error)
    } finally {
      console.log('PokemonList: ', this.pokemonList)
    }
  }

  async onPokemonClicked(pokemon: Pokemon) {
    try {
      const result: any = await this.pokemonService.getPokemonById(pokemon.name)
      this.selectedPokemon = result
      console.log("Valgt Pokemon", this.selectedPokemon)
      this.pokemonStorageService.savePokemon(this.selectedPokemon)
      this.router.navigateByUrl(`/pokemon/${this.selectedPokemon.name}`)
    } catch (error) {
      console.error("ERROR: ", error)
    }
  }
}